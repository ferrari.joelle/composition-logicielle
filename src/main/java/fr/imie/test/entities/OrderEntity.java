package fr.imie.test.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Joëlle on 6/14/2016.
 */
@Entity
@Table(name = "order", schema = "composants_logiciel", catalog = "")
public class OrderEntity {
    private int id;
    private String reference;
    private Serializable createdAt;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "createdAt")
    public Serializable getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Serializable createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderEntity that = (OrderEntity) o;

        if (id != that.id) return false;
        if (reference != null ? !reference.equals(that.reference) : that.reference != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        return result;
    }
}
