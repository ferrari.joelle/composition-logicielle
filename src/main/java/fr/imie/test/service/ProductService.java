package fr.imie.test.service;

import fr.imie.test.dao.ProductRepository;
import fr.imie.test.entities.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by nicol on 14/06/2016.
 */
@Service
public class ProductService {

    @Autowired
    public ProductRepository productRepository;

    public Iterable<ProductEntity> findAllProducts() {
        return productRepository.findAll();
    }
}
