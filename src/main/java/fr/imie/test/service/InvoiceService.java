package fr.imie.test.service;

import fr.imie.test.dao.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by nicol on 14/06/2016.
 */
@Service
public class InvoiceService {

    @Autowired
    public static InvoiceRepository invoiceRepository;

    public static long findLastInvoiceID() {
        return invoiceRepository.count();
    }
}
