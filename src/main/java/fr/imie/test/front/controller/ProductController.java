package fr.imie.test.front.controller;

import fr.imie.test.entities.ProductEntity;
import fr.imie.test.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by nicol on 14/06/2016.
 */
@Controller
public class ProductController {

    @Autowired
    public ProductService productService;

    @RequestMapping(path = "/api/products/list", method = RequestMethod.GET)
    public Iterable<ProductEntity> findAll(){
        return productService.findAllProducts();
    }

}
