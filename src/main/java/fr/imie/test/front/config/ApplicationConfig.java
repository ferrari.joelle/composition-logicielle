package fr.imie.test.front.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by nicol on 14/06/2016.
 */
@SpringBootApplication
@ComponentScan(basePackages = {"config", "controller", "fr"})
@EnableJpaRepositories(basePackages = {"fr"})
@EntityScan(basePackages = {"fr"})
public class ApplicationConfig  {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
    }
}
/*public class ApplicationConfig extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
        application.bannerMode(Banner.Mode.OFF);
        return application.sources(ApplicationConfig.class);
    }
}*/
