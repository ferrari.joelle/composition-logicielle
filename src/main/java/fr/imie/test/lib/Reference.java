package fr.imie.test.lib;

import fr.imie.test.dao.InvoiceRepository;
import fr.imie.test.entities.InvoiceEntity;
import fr.imie.test.service.InvoiceService;
import fr.imie.test.service.ProductService;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by nicol on 16/06/2016.
 */
public class Reference {

    private SecureRandom random = new SecureRandom();

    public String RdStr() {
        return new BigInteger(130, random).toString(32);
    }

    public String generateOrder() {
        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        String random = RdStr();

        String res = "order_" + ts + random;

        return res;
    }

    public String generateDelivery() {
        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        String random = RdStr();

        String res = "delivery_" + ts + random;

        return res;
    }

    public String generateInvoice() {
        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        String random = RdStr();
        long nb = InvoiceService.findLastInvoiceID() + 1;

        String res = "invoice" + ts + random + "_" + nb;

        return res;
    }
}
