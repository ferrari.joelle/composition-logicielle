package fr.imie.test.dao;

import fr.imie.test.entities.ProductEntity;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by nicol on 14/06/2016.
 */
public interface InvoiceRepository extends CrudRepository<ProductEntity, Integer> {


    //@Override
    //Iterable<ProductEntity> findAll();
}
